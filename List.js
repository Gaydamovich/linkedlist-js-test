class NodeList {
  constructor(value, next = null) {
    this.value = value
    this.next = next
  }
}

class LinkedList {
  #size = 0

  constructor() {
    this.head = null
    this.tail = null

    LinkedList.count++
  }

  toString() {
    return `[object ${this.constructor.name}]`
  }

  static count = 0

  static isEmpty(list) {
    return (typeof list === "object" && list) ? !list.length : null
  }

  /** Превращает список в массив */
  static switchToArray(list) {
    const array = []

    let current = list.head

    while (current) {
      array.push(current)
      current = current.next
    }

    return array
  }

  /** Превращает список в массив значений */
  static getArrayValues(list) {
    if (!list) return null
    return LinkedList.switchToArray(list).map(el => el.value)
  }

  /** Превращает список в объект */
  static switchToMap(list) {
    const object = {}
    let count = 0

    let current = list.head

    while (current) {
      object[count] = current
      current = current.next
      count++
    }

    return object
  }

  get last() {
    return this.tail
  }

  get first() {
    return this.head
  }

  get length() {
    return this.#size
  }

  push(value) {
    if (!value) return null

    const newNode = new NodeList(value)

    if (this.length === 0) {
      this.head = newNode
      this.tail = newNode
      this.#size++
    } else {
      this.tail.next = newNode
      this.tail = newNode
      this.#size++
    }

    return this
  }

  pop() {
    if (this.length > 1) {
      const current = this.tail
      let prev = this.head

      while (prev.next !== current) {
        prev = prev.next
      }

      prev.next = null
      this.tail = prev
      this.#size--
      return this
    } else if (this.length === 1) {
      this.head = this.tail = null
      this.#size = 0
      return this
    }
  }

  shift() {
    if (this.length > 1) {
      this.head = this.head.next
      this.#size--
      return this
    } else if (this.length === 1) {
      this.head = this.tail = null
      this.#size = 0
      return this
    }
  }

  unshift(value) {
    if (!value && typeof value !== "number") return null

    const newNode = new NodeList(value)

    if (this.length === 0) {
      this.head = newNode
      this.tail = newNode
      this.#size++
    } else {
      newNode.next = this.head
      this.head = newNode
      this.#size++
    }

    return this
  }

  findById(index) {
    if (typeof index !== 'number') throw TypeError('expected type number')
    if (index >= this.length || index < 0) return undefined

    let count = 0
    let current = this.head

    while (count !== index) {
      current = current.next
      count++
    }

    return current
  }

  deleteById(index) {
    if (typeof index !== 'number') throw TypeError('expected type number')
    if (index >= this.length || index < 0) return undefined

    let prev, next
    let current = this.head
    let count = 0

    while (count !== index) {
      if (index - count === 1) {
        prev = current
      }
      current = current.next
      count++
    }

    next = current.next
    prev.next = next
    this.#size--

    return current
  }

  revers() {
    let current = this.head
    let prev = null
    let next = null

    while (current) {
      next = current.next
      current.next = prev
      prev = current
      current = next
    }

    this.tail = this.head
    this.head = prev

    return this
  }

  clear() {
    this.head = this.tail = null
    this.#size = 0

    return this
  }
}

LinkedList.prototype.__proto__ = null